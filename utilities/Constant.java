// SWAT v1.0
// Test Automation Framework: Deem Ground App
// Utility: Constant	
// Author: Vamshi Bedhadi
// Description: 

package utilities;

public class Constant {

		//Selenium Configurations
		public static final int timeout = 50;
	
		//App Configurations
		public static final String URL = "https://www.google.com";
		public static final String Path_TestData = "C://SWAT - Google//src//testData//";
		public static final String File_TestData = "TestData.xlsx";
		public static final String Path_ScreenShot = "C://SWAT - Google//src//screenshots//";
		
		//Test Data Sheet Columns
		public static final int Col_TestCaseName = 0;	
		public static final int Col_UserName =1 ;
		public static final int Col_Password = 2;
		public static final int Col_Browser = 3;
		public static final int Col_ServiceType = 4;
		public static final int Col_PickupType = 5;
		public static final int Col_Pickup = 6;
		public static final int Col_StopType = 7;
		public static final int Col_Stop = 8;
		public static final int Col_DropoffType = 9;
		public static final int Col_Dropoff = 10;
		public static final int Col_PickupDate = 11;
		public static final int Col_PickupHour = 12;
		public static final int Col_PickupMinute = 13;
		public static final int Col_PickupAmPm = 14;
		public static final int Col_DropoffDate = 15;
		public static final int Col_DropoffHour = 16;
		public static final int Col_DropoffMinute = 17;
		public static final int Col_DropoffAmPm = 18;
		public static final int Col_Result = 19;
		
	}
