// SWAT v1.0
// Test Automation Framework: Deem Ground App
// Driver Script: Framework_001	
// Author: Vamshi Bedhadi
// Description: 

package driverScript;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import testCases.PageValidations;
import testCases.TC_04_Logout;
import testCases.TC_08_New_AirportParking;
import testObjects.BaseClass;
import utilities.Constant;
import utilities.ExcelUtils;
import utilities.Log;
import utilities.Utils;

public class Framework_002{
	public WebDriver driver;
	private String sTestCaseName;
	private int iTestCaseRow;

  //@BeforeMethod - Prerequisites of the main Test Case	
  @BeforeMethod
  public void beforeMethod() throws Exception {
	    //Configuring Log4j Logging
	  	DOMConfigurator.configure("log4j.xml");
	  	
	  	//Reading Test Case name and row from the Test Data Excel sheet
	  	sTestCaseName = this.toString();
	  	sTestCaseName = Utils.getTestCaseName(this.toString());
	  	
	  	//Start printing logs
		Log.startTestCase(sTestCaseName);
		
		//Setting up Test Data Excel
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"AirportParking");
		
		//Fetching Test Case row number from Test Data Sheet
		iTestCaseRow = ExcelUtils.getRowContains(sTestCaseName,Constant.Col_TestCaseName);
		
		//Launching the Browser
		driver = Utils.OpenBrowser(iTestCaseRow);
		
		//Initializing Base Class for Selenium driver
		new BaseClass(driver);  
        }
  
  //@Test - Starting of the main Test Case
  @Test
  public void main() throws Exception {
	  try{  
		//List of Test Cases to execute
		  //PageValidations.Execute2(iTestCaseRow);
		  TC_08_New_AirportParking.Execute8(iTestCaseRow);
		  TC_04_Logout.Execute4(iTestCaseRow);
			
		//Writing test results (Pass or Fail) back to the excel sheet
		if(BaseClass.bResult==true){
			ExcelUtils.setCellData("Pass", iTestCaseRow, Constant.Col_Result);
		}else{
			throw new Exception("Test Case Failed");
		}
		
	  }catch (Exception e){
		//Writing test results (Pass or Fail) back to the excel sheet
		  ExcelUtils.setCellData("Fail", iTestCaseRow, Constant.Col_Result);
		  
		  //If an exception is encountered between the test, then take a screen shot
		  Utils.takeScreenshot(driver, sTestCaseName);
		  
		  //Print the error log message
		  Log.error(e.getMessage());
		  throw (e);
	  }
		
  }
		
  //@AfterMethod - Close the test case		
  @AfterMethod
  public void afterMethod() {
	    // Print logs
	    Log.endTestCase(sTestCaseName);
	    
	    //Close the driver
	    driver.quit();
  		}

}
