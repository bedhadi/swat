// SWAT v1.0
// Test Automation Framework: Deem Ground App
// Test Case: TC_01_Login	
// Author: Vamshi Bedhadi
// Description: 

package testCases;
import org.testng.Reporter;
import testObjects.HomePage;
import testObjects.Booking_Page;
import testObjects.Login_Page;
import utilities.Constant;
import utilities.ExcelUtils;
import utilities.Log;

    public class PageValidations {
		public static void HomePage(int iTestCaseRow) throws Exception{
			org.testng.Assert.assertEquals(true, HomePage.lnk_Gmail().isDisplayed());
			org.testng.Assert.assertEquals(true, HomePage.lnk_Images().isDisplayed());
			
        	org.testng.Assert.assertEquals(true, HomePage.txt_SearchBox().isDisplayed());
        	
            Reporter.log("* Login successful with Company\n");
            
        }
    }
