// SWAT v1.0
// Test Automation Framework: Deem Ground App
// Test Case: TC_01_Login	
// Author: Vamshi Bedhadi
// Description: 

package testCases;
import org.testng.Reporter;

import testObjects.Booking_Page;
import testObjects.HomePage;
import testObjects.Login_Page;
import utilities.Constant;
import utilities.ExcelUtils;
import utilities.Log;

    public class SignIn {
        public static void Execute1(int iTestCaseRow) throws Exception{
        	//iTestcaseRow = Row number of the Test Case name in Test Data sheet
        	//Constant.Col_UserName = Column number for UserName column in Test Data sheet
        	String sUserName = ExcelUtils.getCellData(iTestCaseRow, Constant.Col_UserName);
            
        	HomePage.btn_SignIn().click();
        	Login_Page.txt_UserName().sendKeys(sUserName);
            Log.info(sUserName+" is entered in UserName text box" );
            
            String sPassword = ExcelUtils.getCellData(iTestCaseRow, Constant.Col_Password);
            Login_Page.txt_Password().sendKeys(sPassword);
            Log.info(sPassword+" is entered in Password text box" );
            
            Login_Page.btn_LogIn().click();
            Log.info("Click action is performed on Submit button");
            
            String Company = Booking_Page.txt_Company().getText();
            String User = Booking_Page.txt_User().getText();
                      		
            Reporter.log("* Login successful with Company: " + Company + " and User:" + User + "\n");
            Reporter.log(" " + "\n");
        }
    }
