// SWAT v1.0
// Test Automation Framework: Deem Ground App
// Test Case: TC_04_Logout	
// Author: Vamshi Bedhadi
// Description: 

package testCases;
import testObjects.Booking_Page;
import utilities.Log;

    public class TC_04_Logout {
        public static void Execute4(int iTestCaseRow) throws Exception{
            Booking_Page.lnk_Logout().click();
            Thread.sleep(2000);
            Log.info("Click action is performed on Logout link");
            
        }
    }
