// SWAT v1.0
// Test Automation Framework: Deem Ground App
// Object Repository: Accounting_Page		
// Author: Vamshi Bedhadi
// Description: 
// Page Object Model (POM)

package testObjects;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.Constant;
import utilities.Log;
 
public class HomePage extends BaseClass {
	private static WebElement element = null;
    public HomePage(WebDriver driver){
    super(driver);
    } 
			public static WebElement lnk_Gmail() throws Exception{
			try{
       		WebDriverWait wait = new WebDriverWait(driver, Constant.timeout); 
        	element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='gbw']/div/div/div[1]/div[1]/a")));
       		Log.info("Gmail link is displayed on Home Page");
			}catch (Exception e){
       		Log.error("Gmail link is NOT displayed on Home Page");      		
       		throw(e);
       		}
			return element;
			}
    
			public static WebElement lnk_Images() throws Exception{
				try{
	       		WebDriverWait wait = new WebDriverWait(driver, Constant.timeout); 
	        	element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='gbw']/div/div/div[1]/div[2]/a")));
	       		Log.info("Images link is displayed on Home Page");
				}catch (Exception e){
	       		Log.error("Images link is NOT displayed on Home Page");      		
	       		throw(e);
	       		}
				return element;
				}
			
    	    public static WebElement btn_SignIn() throws Exception{
                try{
                	WebDriverWait wait = new WebDriverWait(driver, Constant.timeout); 
                	element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='gb_70']")));      
    	            Log.info("Sign In button is displayed on Home Page");
                }catch (Exception e){
                	Log.error("Sign In button is NOT displayed on Home Page");
               		throw(e);
               		}
               	return element;
    	    }
    	    
    	    public static WebElement img_Doodle() throws Exception{
                try{
                	WebDriverWait wait = new WebDriverWait(driver, Constant.timeout); 
                	element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='hplogop']")));      
    	            Log.info("Doodle is displayed on Home Page");
                }catch (Exception e){
                	Log.error("Doodle is NOT displayed on Home Page");
               		throw(e);
               		}
               	return element;
    	    }
    	    
    		public static WebElement txt_SearchBox() throws Exception{
           	try{
           		WebDriverWait wait = new WebDriverWait(driver, Constant.timeout); 
            	element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='lst-ib']")));
           		Log.info("Search Box is displayed on Home Page");
           	}catch (Exception e){
           		Log.error("Search Box is NOT displayed on Home Page");      		
           		throw(e);
           		}
           	return element;
            }
    
    	    public static WebElement btn_GoogleSearch() throws Exception{
                try{
                	WebDriverWait wait = new WebDriverWait(driver, Constant.timeout); 
                	element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='tsf']/div[2]/div[3]/center/input[1]")));      
    	            Log.info("Google Search button is displayed on Home Page");
                }catch (Exception e){
                	Log.error("Google Search button is NOT displayed on Home Page");
               		throw(e);
               		}
               	return element;
    	    }
    	    
    	    public static WebElement btn_ImFeelingLucky() throws Exception{
                try{
                	WebDriverWait wait = new WebDriverWait(driver, Constant.timeout); 
                	element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='gbqfbb']")));      
    	            Log.info("I'm Feeling Lucky button is displayed on Home Page");
                }catch (Exception e){
                	Log.error("I'm Feeling Lucky button is NOT displayed on Home Page");
               		throw(e);
               		}
               	return element;
    	    }
    	    
    	    public static WebElement lnk_Advertising() throws Exception{
                try{
                	WebDriverWait wait = new WebDriverWait(driver, Constant.timeout); 
                	element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='fsl']/a[1]")));      
    	            Log.info("Advertising link is displayed on Home Page");
                }catch (Exception e){
                	Log.error("Advertising link is NOT displayed on Home Page");
               		throw(e);
               		}
               	return element;
    	    }
 
    	    public static WebElement lnk_Business() throws Exception{
                try{
                	WebDriverWait wait = new WebDriverWait(driver, Constant.timeout); 
                	element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='fsl']/a[2]")));      
    	            Log.info("Business link is displayed on Home Page");
                }catch (Exception e){
                	Log.error("Business link is NOT displayed on Home Page");
               		throw(e);
               		}
               	return element;
    	    }

    	    public static WebElement lnk_About() throws Exception{
                try{
                	WebDriverWait wait = new WebDriverWait(driver, Constant.timeout); 
                	element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='fsl']/a[3]")));      
    	            Log.info("About link is displayed on Home Page");
                }catch (Exception e){
                	Log.error("About link is NOT displayed on Home Page");
               		throw(e);
               		}
               	return element;
    	    }
    	    
    	    public static WebElement lnk_Privacy() throws Exception{
                try{
                	WebDriverWait wait = new WebDriverWait(driver, Constant.timeout); 
                	element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='fsr']/a[1]")));      
    	            Log.info("Privacy link is displayed on Home Page");
                }catch (Exception e){
                	Log.error("Privacy link is NOT displayed on Home Page");
               		throw(e);
               		}
               	return element;
    	    }
    	    
    	    public static WebElement lnk_Terms() throws Exception{
                try{
                	WebDriverWait wait = new WebDriverWait(driver, Constant.timeout); 
                	element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='fsr']/a[2]")));      
    	            Log.info("Terms link is displayed on Home Page");
                }catch (Exception e){
                	Log.error("Terms link is NOT displayed on Home Page");
               		throw(e);
               		}
               	return element;
    	    }
    	    
    	    public static WebElement lnk_Settings() throws Exception{
                try{
                	WebDriverWait wait = new WebDriverWait(driver, Constant.timeout); 
                	element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='fsettl']")));      
    	            Log.info("Settings link is displayed on Home Page");
                }catch (Exception e){
                	Log.error("Settings link is NOT displayed on Home Page");
               		throw(e);
               		}
               	return element;
    	    }
}
      	