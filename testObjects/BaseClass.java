// SWAT v1.0
// Test Automation Framework: Deem Ground App
// Object Repository: BaseClass		
// Author: Vamshi Bedhadi
// Description: 

package testObjects;
import org.openqa.selenium.WebDriver;

public class BaseClass {
	public static WebDriver driver;
	public static boolean bResult;
	public BaseClass(WebDriver driver){
		BaseClass.driver = driver;
		BaseClass.bResult = true;
		BaseClass.driver.manage().window().maximize();
	}

}
