![](swat-logo.png)
# SWAT<br>

##### Overview<br>
SWAT is a test automation framework for web applications, API, and services built using Java, Selenium 
WebDriver, ANT, TestNG, and Eclipse. This framework uses Page Object Model�(POM) approach to help enable code efficiency, 
reusability, and readability.
<br>
<br>

##### Copyright<br>
� 2013 - 2022 Vamshi Bedhadi